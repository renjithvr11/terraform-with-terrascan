# Terraform scripts scanning with Terrascan

## Setup Requirements
- Container image containing Terrascan
- Provisioned Gitlab Kubernetes Runners

## Notes
This is a small example showing how we can leverage Terrascan static code analyser to scan Terraform scripts and output the results within Pipeline - Tests Results Tab

## References
- https://github.com/tenable/terrascan
